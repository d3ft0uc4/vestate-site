# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='RecommendedListing',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('bpm_id', models.CharField(max_length=50, verbose_name='bpm_id')),
                ('name', models.CharField(max_length=250, verbose_name='name')),
                ('address', models.CharField(max_length=250, verbose_name='address')),
                ('price', models.CharField(max_length=250, verbose_name='price')),
                ('timestamp', models.IntegerField(verbose_name='timestamp')),
            ],
        ),
    ]
