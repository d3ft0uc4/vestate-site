# coding=utf-8
from email.mime.text import MIMEText

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.views.generic import TemplateView, FormView
from realestate.listing.models import *
from realestate.home.models import *
from realestate.listing.services import calc_distance
from realestate.services import *
import time
from django.views.decorators.csrf import csrf_exempt
import smtplib
import realestate.settings as app_settings


class BaseTemplateView(TemplateView):
    def get_context_data(self, **kwargs):
        context = super(BaseTemplateView, self).get_context_data(**kwargs)
        blog = get_blog()
        context['blog'] = blog
        return context


class IndexView(BaseTemplateView, FormView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        search = self.request.session.get('search', {})
        context = super(IndexView, self).get_context_data(**kwargs)
        listings_count = Listing.objects.count()
        if search:
            l = int(search['lower']) * 1000000
            u = int(search['upper']) * 1000000
            listings = sorted(Listing.objects.filter(Price__gte=l * 0.8, Price__lte=u * 1.2),
                              key=lambda o: calc_distance(search, o))[:10]
            search['price_search'] = 10.0
            listings_bottom = sorted(Listing.objects.filter(),
                                     key=lambda o: calc_distance(search, o))[:8]
        else:
            listings = Listing.objects.all()[:10]
            listings_bottom = sorted(Listing.objects.all(), key=lambda o: 10 - o.pricecriteria)[:8]

        context['listings'] = listings
        context['listings_bottom'] = listings_bottom
        context['listing_count'] = listings_count
        context['search'] = json.dumps(search)
        return context


@csrf_exempt
def search(request):
    if request.method == 'POST':
        json_data = json.loads(request.body)
        print json_data
        json_data['lower'] = int(float(filter(lambda x: x in '0123456789.', json_data['lower'])))
        json_data['upper'] = int(float(filter(lambda x: x in '0123456789.', json_data['upper'])))
        request.session['search'] = json_data
        return HttpResponse('')


@csrf_exempt
def handle_forms(request):
    if request.method == 'POST':
        print request.body
        json_data = json.loads(request.body)
        msg_str = u'Тема: {0}\nИмя: {1}\nEmail:{2}\nТелефон:{3}\nКомментарий:{4}' \
            .format(
            json_data['tag'],
            json_data['name'],
            json_data['email'],
            json_data['phone'],
            json_data['message'],
            )
        request.session['form_submitted'] = True
        server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        msg = MIMEText(msg_str.encode('utf-8'), _charset='utf-8')
        server.ehlo()
        server.login(app_settings.GMAIL_USER, app_settings.GMAIL_PASSWORD)
        server.sendmail(app_settings.GMAIL_USER, app_settings.MAIL_RECEIVER, msg.as_string())
        server.quit()
        return HttpResponse()


def handle404(request):
    response = render_to_response(
        '404.html',
        context_instance=RequestContext(request)
    )
    response.status_code = 404

    return response


class ContactView(BaseTemplateView):
    template_name = 'home/contact-us.html'


class SubmitView(BaseTemplateView):
    template_name = 'home/submit.html'


class BlogView(BaseTemplateView):
    template_name = 'home/blog.html'

    def get_context_data(self, **kwargs):
        context = super(BlogView, self).get_context_data(**kwargs)
        blog = get_blog(row_count=5, fields=['Id', 'Name', 'CreatedOn', 'Notes', 'Type'])
        context['blog_full'] = blog
        return context
