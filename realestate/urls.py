from django.conf.urls import *
from django.conf import settings
from django.contrib import admin
from realestate.listing import views as listing_views
from realestate.home.views import *
from django.conf.urls import handler400, handler403, handler404, handler500

admin.autodiscover()

handler404 = handle404
# handler500 = 'my_app.views.server_error'

urlpatterns = patterns(
    '',
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^sync_bpm/(?P<listing_id>[0-9A-Fa-f-]+)/$', sync_single_listing, name='sync_single_listing'),
    url(r'^search/$', search, name='search'),
    url(r'^listings/$', listing_views.ListingList.as_view(), name='listings'),
    url(r'^mortgages/$', SubmitView.as_view(), name='mortgages'),
    url(r'^listings/(?P<slug>[\w-]+)/', listing_views.ListingView.as_view(), name='listing_details'),
    url(r'^agents/$', listing_views.AgentList.as_view(), name='agents'),
    url(r'^contact/$', ContactView.as_view(), name='contact'),
    url(r'^blog/$', BlogView.as_view(), name='blog'),
    url(r'^handle_forms/$', handle_forms, name='handle_forms'),

    # Static Pages
    (r'^i18n/', include('django.conf.urls.i18n')),

)

if settings.DEBUG:
    urlpatterns += patterns(
        '',
        (r'^media/(.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
        (r'^static/(.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    )
