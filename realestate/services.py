# coding=utf-8
from django.http import HttpResponse
from realestate.bpm.constants import *
from realestate.bpm.helper_classes import *
from realestate.exceptions import SecurityException
from realestate.listing.models import *
from . import settings as app_settings
from realestate.bpm import constants
import requests
import json
import datetime
from dateutil import parser
import time

headers = {'Content-Type': 'application/json'}

columns = {
    'Listing': ['Id',
                'Name',
                'Status',
                'ListingType',
                'Price',
                'Notes',
                'PropertyCategory',
                'PropertyType',
                'Description',
                'Address',
                'Latitude',
                'Longitude',
                'UsrRoad',
                'UsrBOOLEANIsPrivate',
                'UsrBOOLEANIsPublic',
                'UsrBOOLEANIsSite',
                'UsrVerdict',
                'UsrMaterial',
                'UsrDistanceFromMkad',
                'UsrOriginalID',
                'UsrRatingCoefficient',
                'Currency',
                'UsrBOOLEANIsPhotos'],
    'ListingGalleryImage': ['Id', 'Listing', 'Position', 'Link'],
    'AmenityInObject': ['Id',
                        'Listing',
                        'Amenity',
                        'Value',
                        'IntValue',
                        'FloatValue',
                        'BooleanValue',
                        'StringValue',
                        'DateValue']

}


def auth():
    base_url = app_settings.BPM_BASE_URL
    username = app_settings.BPM_USERNAME
    password = app_settings.BPM_PASSWORD
    credentials = {
        'UserName': username,
        'UserPassword': password
    }
    auth_url = get_auth_url(base_url)

    session = requests.session()
    auth = session.post(url=auth_url, data=json.dumps(credentials), headers=headers, allow_redirects=False)
    csrf_token = session.cookies.get_dict().get('BPMCSRF', None)
    if auth.status_code == 200:
        return AuthHolder(session, csrf_token)
    else:
        raise SecurityException(auth.text)


def get_sys_image(guid):
    auth_holder = auth()
    url = get_sys_image_url(app_settings.BPM_BASE_URL, guid)
    h = headers
    h['BPMCSRF'] = auth_holder.token
    response = auth_holder.session.get(url, headers=headers, allow_redirects=False)
    return response.content


def send_to_bpm(query):
    auth_holder = auth()
    url = get_query_endpoint(app_settings.BPM_BASE_URL, 'Select')
    h = headers
    h['BPMCSRF'] = auth_holder.token
    response = auth_holder.session.post(url, data=query, headers=headers, allow_redirects=False).json()
    if app_settings.DEBUG:
        print response
    return response['rows']


def get_agents():
    query = select_entities('Employee', columns=['Id', 'Name', 'Contact', 'Job'])
    employees = send_to_bpm(query)
    for e in employees:
        query = select_by_column('ContactCommunication', filters={'Contact': e['Contact']['value']}, columns=['Number'])
        communications = send_to_bpm(query)
        query = select_by_column('Contact', filters={'Id': e['Contact']['value']}, columns=['Photo'])
        photo = send_to_bpm(query)[0]['Photo']
        if photo:
            ph_id = photo['value']
            p = get_sys_image(ph_id)
            e['Photo'] = p.encode("base64")
        e['Communications'] = communications
        if e['Job']:
            e['Job'] = e['Job']['displayValue']
    return employees


def get_blog(row_count=3, page_num=0, fields=['Id', 'Name', 'CreatedOn', 'Type']):
    try:
        query = select_entities('KnowledgeBase', columns=fields, row_count=row_count,
                                page_num=page_num)

        rows = send_to_bpm(query)
        for r in rows:
            r['CreatedOn'] = human_readable_date(r['CreatedOn'])
        return rows
    except:
        return []


def human_readable_date(date_str):
    dt = parser.parse(date_str) + datetime.timedelta(hours=3)
    months = {'Jan': 'января',
              'Feb': 'февраля',
              'Mar': 'марта',
              'Apr': 'апреля',
              'May': 'мая',
              'Jun': 'июня',
              'Jul': 'июля',
              'Aug': 'августа',
              'Sep': 'сентября',
              'Oct': 'октября',
              'Nov': 'ноября',
              'Dec': 'декабря'}
    dt = dt.strftime("%d %b в %H:%M")
    for k, v in months.items():
        dt = dt.replace(k, str(v))
    return dt


def get_rates():
    return json.loads(requests.get('http://free.currencyconverterapi.com/api/v5/convert?q=USD_RUB&compact=y').text)[
        'USD_RUB'][
        'val']


def get_object_filter(esn, filters):
    query = select_by_column(esn, filters=filters,
                             columns=columns[esn])
    return send_to_bpm(query)


def get_objects(esn):
    count = send_to_bpm(count_entities(esn))[0]['IdCount']
    objs = []
    for page_num in xrange((count / 20000) + 1):
        query = select_entities(esn,
                                columns=columns[esn],
                                row_count=20000, page_num=page_num)
        objs.extend(send_to_bpm(query))
    return objs


def save_listings(listings, rates):
    for l in listings:
        obj = Listing()
        isValid = obj.from_json(l, rates)
        Listing.objects.filter(bpm_id=l['Id']).delete()
        if isValid:
            obj.save()


def save_listing_images(imgs):
    listings = map(lambda x: x.bpm_id, Listing.objects.all())
    for i in imgs:
        if i['Listing'] and i['Listing']['value'] and i['Listing']['value'] in listings:
            obj = ListingImage()
            isValid = obj.from_json(i)
            ListingImage.objects.filter(bpm_id=i['Id']).delete()
            if isValid:
                obj.save()


def save_amenity_in_objs(aio):
    listings = map(lambda x: x.bpm_id, Listing.objects.all())
    for i in aio:
        obj = AmenityInObject()
        if i['Listing'] and i['Listing']['value'] and i['Listing']['value'] in listings:
            isValid = obj.from_json(i)
            AmenityInObject.objects.filter(bpm_id=i['Id']).delete()
            if isValid:
                obj.save()


def sync_listings(listing_id=None):
    t1 = time.time()
    esn = 'Listing'
    rates = get_rates()
    if listing_id:
        listings = get_object_filter(esn, {'Id': listing_id})
    else:
        listings = get_objects(esn=esn)
        Listing.objects.exclude(bpm_id__in=map(lambda o: o['Id'], listings)).delete()
    save_listings(listings, rates)
    print "Sync listing complete in {} sec".format(int(time.time() - t1))


def sync_listing_images(listing_id=None):
    t1 = time.time()
    esn = 'ListingGalleryImage'
    if listing_id:
        imgs = get_object_filter(esn, {'Listing': listing_id})
    else:
        imgs = get_objects(esn=esn)
    save_listing_images(imgs)
    print "Sync listing images complete in {} sec".format(int(time.time() - t1))


def sync_amenity():
    esn = 'Amenity'
    t1 = time.time()
    ob = []
    query = select_entities(esn, columns=[])
    ob.extend(send_to_bpm(query))
    for o in ob:
        obj = Amenity()
        isValid = obj.from_json(o)
        Amenity.objects.filter(bpm_id=obj.bpm_id).delete()
        if isValid:
            obj.save()
    print "Sync amenity complete in {} sec".format(int(time.time() - t1))


def sync_amenity_in_object(listing_id=None):
    t1 = time.time()
    esn = 'AmenityInObject'
    if listing_id:
        aio = get_object_filter(esn, {'Listing': listing_id})
    else:
        aio = get_objects(esn=esn)
    save_amenity_in_objs(aio)
    print "Sync amenity in objects complete in {} sec".format(int(time.time() - t1))


def sync_price_per_sqm():
    amenityinobjects = AmenityInObject.objects.filter(Amenity=Amenity.objects.get(Name='Площадь дома'))
    price_per_sqm = []
    for o in amenityinobjects:
        price = o.Listing.Price
        sqm = float(o.Value)
        if sqm > 0.0 and price > 0:
            price_per_sqm.append(price / sqm)
    if len(price_per_sqm) == 0:
        return
    max_price_per_sqm = sorted(price_per_sqm)[-1]
    for o in amenityinobjects:
        price = o.Listing.Price
        sqm = float(o.Value)
        if sqm > 0.0 and price > 0:
            p = price / sqm
            listing = o.Listing
            listing.pricecriteria = int(p / max_price_per_sqm * 10)
            listing.save()


def sync(r):
    sync_listings()
    sync_listing_images()
    sync_amenity()
    sync_amenity_in_object()
    sync_price_per_sqm()
    return 'ok'


def sync_single_listing(request, listing_id):
    sync_key = request.GET.get('sync_key')
    print sync_key
    print app_settings.SYNC_KEY
    if sync_key != app_settings.SYNC_KEY:
        return HttpResponse(status=401)
    t1 = time.time()
    sync_listings(listing_id)
    sync_listing_images(listing_id)
    sync_amenity_in_object(listing_id)
    sync_price_per_sqm()
    print "Sync single object complete in {} sec".format(int(time.time() - t1))
    return HttpResponse('ok')
