# -*- coding: utf-8 -*-
import codecs
import xml.etree.ElementTree as xml
from xml.dom import minidom
import realestate.settings as app_settings
from realestate.services import *


def find(f, seq):
    """Return first item in sequence where f(item) == True."""
    for item in seq:
        if f(item):
            return item


def prettify(elem):
    rough_string = xml.tostring(elem, 'utf-8')
    return rough_string


def get_displayvalue(obj):
    if isinstance(obj, unicode):
        return u''
    else:
        return obj['displayValue']


def get_data():
    return {
        'listings': _get_listings(),
        'imgs': _get_imgs(),
        'amenities': _get_amenities(),
        'amenities_in_obj': _get_amenities_in_obj()
    }


def create_xml(data, tpe='UsrBOOLEANIsPublic'):
    feed = xml.Element("feed")
    ver = xml.Element("feed_version")
    ver.text = '2'
    feed.append(ver)
    listings = data['listings']
    imgs = data['imgs']
    amenities = data['amenities']
    amenities_in_obj = data['amenities_in_obj']
    highways = {
        u'Ильинское': '9',
        u'Калужское': '10',
        u'Киевское': '12',
        u'Минское': '16',
        u'Можайское': '17',
        u'Новорижское': '19',
        u'Рублево - Успенкое ': '27'
    }
    for l in listings:
        if not l[tpe]:
            continue
        object = xml.SubElement(feed, "object")
        ExternalId = xml.SubElement(object, "ExternalId")
        ExternalId.text = l['Id']
        Description = xml.SubElement(object, "Description")
        Description.text = l['Description']
        Address = xml.SubElement(object, "Address")
        Address.text = l['Address']
        if not l['Address']:
            Address.text = u'Москва'
        Coordinates = xml.SubElement(object, "Coordinates")
        Lat = xml.SubElement(Coordinates, 'Lat')
        Lng = xml.SubElement(Coordinates, 'Lng')
        Lat.text = str(l['Latitude'])
        if not l['Latitude']:
            Lat.text = '55.753215'
        Lng.text = str(l['Longitude'])
        if not l['Longitude']:
            Lng.text = '37.622504'
        Phones = xml.SubElement(object, "Phones")
        PhoneSchema = xml.SubElement(Phones, "PhoneSchema")
        CountryCode = xml.SubElement(PhoneSchema, "CountryCode")
        CountryCode.text = '+7'
        Number = xml.SubElement(PhoneSchema, "Number")
        Number.text = '4952302243'
        if tpe != 'UsrBOOLEANIsPublic':
            Number.text = '4952480474'
        road = get_displayvalue(l['UsrRoad'])
        Highway = xml.SubElement(object, "Highway")
        if l['UsrDistanceFromMkad'] > 0:
            HighwayDistance = xml.SubElement(Highway, "Distance")
            HighwayDistance.text = str(l['UsrDistanceFromMkad'])
        if road:
            HighwayId = xml.SubElement(Highway, "Id")
            HighwayId.text = highways[road]
        Photos = xml.SubElement(object, "Photos")
        ph = filter(lambda o: o['Listing']['value'] == l['Id'], imgs)
        for p in ph:
            PhotoSchema = xml.SubElement(Photos, 'PhotoSchema')
            FullUrl = xml.SubElement(PhotoSchema, 'FullUrl')
            FullUrl.text = p['Link']
            IsDefault = xml.SubElement(PhotoSchema, 'IsDefault')
            if p['Position'] == 0:
                IsDefault.text = 'true'
            else:
                IsDefault.text = 'false'
        PropertyType = l['PropertyType']['displayValue']  # Коттедж
        aio = filter(lambda x: x['Listing']['value'] == l['Id'], amenities_in_obj)
        curr_a = {}
        for a in aio:
            value = a['Value']
            name = find(lambda x: x['Id'] == a['Amenity']['value'], amenities)['Name']
            curr_a[name] = value
        p = {
            'Дом': 'houseSale',
            'Дуплекс': 'houseSale',
            'Коттедж': 'cottageSale',
            'Таунхаус': 'townhouseSale',
        }

        Category = xml.SubElement(object, 'Category')
        Category.text = p.get(PropertyType, 'houseSale')
        TotalArea = xml.SubElement(object, 'TotalArea')
        TotalArea.text = curr_a.get(u'Площадь дома', '0.00')
        HasElectricity = xml.SubElement(object, 'HasElectricity')
        HasElectricity.text = 'true'
        HasWater = xml.SubElement(object, 'HasWater')
        HasWater.text = 'true'
        bedrooms = curr_a.get(u'Количество спален')
        if bedrooms:
            BedroomsCount = xml.SubElement(object, 'BedroomsCount')
            BedroomsCount.text = bedrooms
        gas = curr_a.get(u'Газификация')
        if gas:
            HasGas = xml.SubElement(object, 'HasGas')
            if gas == u'Отсутствует':
                HasGas.text = 'false'
            else:
                HasGas.text = 'true'
        drainage = curr_a.get(u'Канализация')
        if drainage:
            HasDrainage = xml.SubElement(object, 'HasDrainage')
            if drainage == u'Отсутствует':
                HasDrainage.text = 'false'
            else:
                HasDrainage.text = 'true'
        security = curr_a.get(u'Охрана')
        if security:
            HasSecurity = xml.SubElement(object, 'HasSecurity')
            if security == u'нет':
                HasSecurity.text = 'false'
            else:
                HasSecurity.text = 'true'
        garage = curr_a.get(u'Гараж')
        if garage:
            HasGarage = xml.SubElement(object, 'HasGarage')
            if garage == u'нет':
                HasGarage.text = 'false'
            else:
                HasGarage.text = 'true'
        Building = xml.SubElement(object, 'Building')
        floor = curr_a.get(u'Этажей в здании')
        if not floor:
            floor = curr_a.get(u'Количество уровней')
        if floor:
            FloorsCount = xml.SubElement(Building, 'FloorsCount')
            FloorsCount.text = str(floor)
        material = curr_a.get(u'Материал здания')
        if material:
            m = {
                u'Блок': 'brick',
                u'Брус': 'wood',
                u'Дерево': 'wood',
                u'Железобетон': 'brick',
                u'Кирпич': 'brick',
                u'Монолит': 'monolith',
                u'Монолит-кирпич': 'monolith',
                u'Панель': 'brick'
            }
            MaterialType = xml.SubElement(Building, 'MaterialType')
            MaterialType.text = m[material]
        Land = xml.SubElement(object, 'Land')
        Area = xml.SubElement(Land, 'Area')
        AreaUnitType = xml.SubElement(Land, 'AreaUnitType')
        AreaUnitType.text = 'sotka'
        Area.text = str(curr_a.get(u'Площадь участка', '0.00'))
        BargainTerms = xml.SubElement(object, 'BargainTerms')
        Price = xml.SubElement(BargainTerms, 'Price')
        Price.text = str(l['Price'])
        Currency = xml.SubElement(BargainTerms, 'Currency')
        if l['Currency']['value'].lower() == '915e8a55-98d6-df11-9b2a-001d60e938c6':
            Currency.text = 'usd'
        else:
            Currency.text = 'rur'
    fname = 'feed.xml'
    if tpe != 'UsrBOOLEANIsPublic':
        fname = 'feed_private.xml'
    write_xml(prettify(feed), app_settings.FEED_PATH + fname)


def write_xml(str_xml, path):
    with codecs.open(path, "w", "utf-8") as f:
        f.write(str_xml.decode('utf-8'))


def _get_listings():
    esn = 'Listing'
    count = send_to_bpm(count_entities(esn))[0]['IdCount']
    listings = []
    for page_num in xrange((count / 20000) + 1):
        query = select_entities(esn,
                                columns=['Id', 'Name', 'Status', 'ListingType', 'Price', 'Notes', 'PropertyCategory',
                                         'PropertyType', 'Description', 'Address', 'Latitude', 'Longitude', 'UsrRoad',
                                         'UsrBOOLEANIsPrivate', 'UsrBOOLEANIsPublic', 'UsrBOOLEANIsSite', 'UsrVerdict',
                                         'UsrMaterial', 'UsrDistanceFromMkad',
                                         'UsrOriginalID', 'UsrRatingCoefficient', 'Currency'],
                                row_count=20000
                                , page_num=page_num)
        listings.extend(send_to_bpm(query))
    return listings


def _get_imgs():
    esn = 'ListingGalleryImage'
    count = send_to_bpm(count_entities(esn))[0]['IdCount']
    imgs = []
    for page_num in xrange((count / 20000) + 1):
        query = select_entities(esn, columns=['Id', 'Listing', 'Position', 'Link'],
                                row_count=20000,
                                page_num=page_num)
        imgs.extend(send_to_bpm(query))
    return filter(lambda i: i['Listing'], imgs)


def _get_amenities():
    esn = 'Amenity'
    ob = []
    query = select_entities(esn, columns=[])
    ob.extend(send_to_bpm(query))
    return ob


def _get_amenities_in_obj():
    esn = 'AmenityInObject'
    count = send_to_bpm(count_entities(esn))[0]['IdCount']
    aio = []
    for page_num in xrange((count / 20000) + 1):
        query = select_entities(esn,
                                columns=['Id', 'Listing', 'Amenity', 'Value', 'IntValue', 'FloatValue', 'BooleanValue',
                                         'StringValue', 'DateValue'],
                                row_count=20000,
                                page_num=page_num)
        aio.extend(send_to_bpm(query))
    return filter(lambda i: i['Listing'], aio)


if __name__ == "__main__":
    create_xml()
