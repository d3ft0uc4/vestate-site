from django.views.generic.edit import FormMixin
from django.views.generic import ListView, DetailView, View
from realestate.home.views import BaseTemplateView
from realestate.listing.services import calc_distance
from realestate.services import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


class ListingList(BaseTemplateView):
    template_name = 'listing/results.html'

    def get_context_data(self, **kwargs):
        context = super(ListingList, self).get_context_data(**kwargs)
        search = self.request.session.get('search')
        if search:
            l = int(search['lower']) * 1000000
            u = int(search['upper']) * 1000000
            listings = Listing.objects.filter(Price__gte=l * 0.8, Price__lte=u * 1.2)
            listings = sorted(listings,
                              key=lambda o: calc_distance(search, o))
        else:
            listings = Listing.objects.all()
        paginator = Paginator(listings, 24)

        page = self.request.GET.get('page', 1)
        context['listings'] = paginator.page(page)
        context['search'] = json.dumps(search)
        context['head'] = False
        if str(page) == '1':
            context['head'] = True
        return context


class ListingView(FormMixin, DetailView):
    template_name = 'listing/listing-detail.html'
    model = Listing

    def get_object(self, queryset=None):
        return super(ListingView, self).get_object(queryset)

    def get_context_data(self, **kwargs):
        context = super(ListingView, self).get_context_data(**kwargs)
        form_submitted = self.request.session.get('form_submitted', False)
        context['form_submitted'] = form_submitted
        if form_submitted:
            self.request.session['form_submitted'] = False
        context['blog'] = get_blog()
        obj = self.get_object()
        context['listing'] = obj
        suggested = []
        step = 0.1
        while len(suggested) != 5:
            suggested = Listing.objects.filter(Price__gte=obj.Price * (1 - step),
                                               Price__lte=obj.Price * (1 + step)).exclude(
                bpm_id=obj.bpm_id)[:5]
            step += 0.1
        context['suggested'] = suggested
        return context


class AgentList(BaseTemplateView):
    template_name = 'listing/agents.html'

    def get_context_data(self, **kwargs):
        context = super(AgentList, self).get_context_data(**kwargs)
        context['agents'] = get_agents()
        return context
