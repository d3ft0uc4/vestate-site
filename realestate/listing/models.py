# -*- coding: utf-8 -*-
from decimal import Decimal
import os
import re
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import models
from django.template.defaultfilters import slugify
import time
from django.utils.translation import ugettext_lazy as _
from sorl.thumbnail import ImageField
from django.contrib.staticfiles.templatetags.staticfiles import static
import random
from unidecode import unidecode

TYPES = (
    ('house', _('Houses')),
    ('villa', _('Villas')),
    ('penthouse', _('Penthouses')),
    ('apartment', _('Apartments')),
    ('residencial-land', _('Residential Land')),
    ('corporate-office', _('Corporate Offices')),
    ('commercial-office', _('Commercial Offices')),
    ('commercial-space', _('Commercial Space')),
    ('industrial-building', _('Industrial Buildings')),
    ('commercial-warehouses', _('Commercial Warehouses')),
    ('commercial-land', _('Commercial Land')),
)

LOCATION_STREET = 'street'
LOCATION_SECTOR = 'sector'
LOCATION_CITY = 'city'
LOCATION_STATE = 'state'

LOCATION_TYPES = (
    (LOCATION_STREET, _('Street')),
    (LOCATION_SECTOR, _('Sector')),
    (LOCATION_CITY, _('City')),
    (LOCATION_STATE, _('State/Province')),
)

OFFERS = (
    ('buy', _('For Sale')),
    ('rent', _('For Rent')),
    ('buy-rent', _('For Sale/For Rent'))
)

VALIDATIONS = [
    ('realestate.listing.utils.validation_simple', _('One or more characters')),
    ('realestate.listing.utils.validation_integer', _('Integer')),
    ('realestate.listing.utils.validation_yesno', _('Yes/No')),
    ('realestate.listing.utils.validation_decimal', _('Decimal')),
]


class LocationManager(models.Manager):
    def states(self, **kwargs):
        return self.filter(location_type=LOCATION_STATE, **kwargs)

    def cities(self, **kwargs):
        return self.filter(location_type=LOCATION_CITY, **kwargs)

    def sectors(self, **kwargs):
        return self.filter(location_type=LOCATION_SECTOR, **kwargs)

    def streets(self, **kwargs):
        return self.filter(location_type=LOCATION_STREET, **kwargs)


class Location(models.Model):
    parent = models.ForeignKey('self', verbose_name=_('Location'), null=True,
                               blank=True)
    name = models.CharField(_('Name'), max_length=60)
    location_type = models.CharField(_('Location Type'),
                                     choices=LOCATION_TYPES,
                                     default=LOCATION_SECTOR, max_length=20)

    objects = LocationManager()

    def __unicode__(self):
        location_tree = self.get_parent_name(self, [])
        return ', '.join(location_tree)

    def __str__(self):
        return self.__unicode__()

    def get_parent_name(self, location, names):
        names.append(location.name)
        if location.parent is None:
            return names
        return self.get_parent_name(location.parent, names)


class AgentManager(models.Manager):
    def active(self, **kwargs):
        return self.filter(active=True, **kwargs)

    def with_listings(self, **kwargs):
        return self.active(listing__isnull=False, **kwargs)


class Agent(models.Model):
    first_name = models.CharField(max_length=30, verbose_name=_('First name'))
    last_name = models.CharField(max_length=30, verbose_name=_('Last name'))
    phone = models.CharField(max_length=15, verbose_name=_('Phone'), null=True, blank=True)
    mobile = models.CharField(max_length=15, verbose_name=_('Cellphone'), null=True, blank=True)
    location = models.ForeignKey(Location, verbose_name=_('Location'), null=True, blank=True)
    address = models.CharField(max_length=200, verbose_name=_('Address'), null=True, blank=True)
    image = ImageField(upload_to='agents/', default='', verbose_name=_('Picture'), null=True, blank=True)
    user = models.OneToOneField(User, verbose_name=_('User'), null=True, blank=True)
    active = models.BooleanField(default=False, verbose_name=_('Active'))

    objects = AgentManager()

    @property
    def name(self):
        return '%s %s' % (self.first_name, self.last_name)

    @property
    def email(self):
        return self.user.email if self.user is not None else None

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Agent')
        verbose_name_plural = _('Agents')


class ListingManager(models.Manager):
    pass


colors = [
    'mdc-bg-purple-300',
    'mdc-bg-red-300',
    'mdc-bg-light-blue-500',
    'mdc-bg-teal-400',
    'mdc-bg-blue-grey-400',
    'mdc-bg-light-green-500',
    'mdc-bg-amber-400'
]


class Listing(models.Model):
    bpm_id = models.CharField(max_length=50, unique=True, primary_key=True, default="")
    Name = models.CharField(max_length=2000, default="")
    Status = models.CharField(max_length=50, default="")
    ListingType = models.CharField(max_length=50, default="")
    Price = models.IntegerField(default=0)
    Notes = models.CharField(max_length=2000, default="")
    PropertyCategory = models.CharField(max_length=50, default="")
    PropertyType = models.CharField(max_length=50, default="")
    Description = models.CharField(max_length=2000, default="")
    Address = models.CharField(max_length=2000, default="")
    Latitude = models.CharField(max_length=200, default="")
    Longitude = models.CharField(max_length=200, default="")
    UsrRoad = models.CharField(max_length=200, default="")
    UsrVerdict = models.CharField(max_length=2000, default="")
    UsrBOOLEANIsPrivate = models.BooleanField()
    UsrMaterial = models.IntegerField()
    slug = models.SlugField(max_length=200, unique=True, blank=False, verbose_name=_('Slug'))
    pricecriteria = models.FloatField()
    distancecriteria = models.FloatField()
    usroriginalid = models.CharField(max_length=200, default="")
    usrratingcoefficient = models.FloatField(default=1.0)

    def from_json(self, obj,rates):

        if not obj['UsrBOOLEANIsSite']:
            return False
        self.bpm_id = obj['Id']
        self.Name = obj['Name']
        if obj['Status']:
            self.Status = obj['Status']['displayValue']
        if obj['ListingType']:
            self.ListingType = obj['ListingType']['displayValue']
        if obj['Currency']['value'].lower() == '915e8a55-98d6-df11-9b2a-001d60e938c6':
            self.Price = obj['Price'] * rates
        else:
            self.Price = obj['Price']
        self.Notes = obj['Notes']
        if obj['PropertyCategory']:
            self.PropertyCategory = obj['PropertyCategory']['displayValue']
        if obj['PropertyType']:
            self.PropertyType = obj['PropertyType']['displayValue']
        self.Description = obj['Description']
        self.Address = obj['Address']
        self.Latitude = obj['Latitude']
        self.Longitude = obj['Longitude']
        if obj['UsrRoad']:
            self.UsrRoad = obj['UsrRoad']['displayValue']
        self.UsrBOOLEANIsPrivate = not obj['UsrBOOLEANIsPhotos']
        self.UsrVerdict = obj['UsrVerdict']
        self.UsrMaterial = obj['UsrMaterial']
        self.pricecriteria = 5
        dc = float(obj['UsrDistanceFromMkad']) / 4.5
        if dc > 10:
            dc = 10
        if obj['UsrDistanceFromMkad'] == 0:
            dc = 5
        self.distancecriteria = dc
        if obj['UsrOriginalID']:
            self.usroriginalid = obj['UsrOriginalID']
        self.usrratingcoefficient = obj['UsrRatingCoefficient']
        return True

    objects = ListingManager()

    @property
    def main_image(self):
        if not self.UsrBOOLEANIsPrivate:
            im = self.images.all().order_by('position')
            if im.count():
                return im[0]
        return {'absolute_url': static('img/no-photo.jpg'), 'position': 0}

    @property
    def image_list(self):
        if not self.UsrBOOLEANIsPrivate and self.images.all().count():
            imgs = [{'url': image.absolute_url, 'position': image.position} for image in
                    self.images.all().order_by('position') if image.absolute_url.strip()]
            if len(imgs) != 0:
                return imgs
        return [{'url': static('img/no-photo.jpg'), 'position': 0}]

    @property
    def amenities_list(self):
        return filter(lambda d: u'на века' not in d['name'] and u'менеджер' not in d['name'],
                      [{'name': amenity.Amenity.Name, 'value': amenity.Value} for amenity in
                       self
                      .amenityinobj
                      .exclude(Value__iexact='нет').filter(BooleanValue=False)])

    @property
    def bool_amenities_list(self):
        return [{'name': amenity.Amenity.Name, 'class': colors[i % 5]} for i, amenity
                in
                enumerate(self.amenityinobj.filter(BooleanValue=True))]

    @property
    def address(self):
        return self.get_address()

    def get_address(self):
        if self.location is None:
            return _('No location provided')
        return self.location

    def __unicode__(self):
        return self.Name

    class Meta:
        verbose_name = _('Listing')
        verbose_name_plural = _('Listings')
        ordering = ['-Price', ]

    def save(self, **kwargs):
        self._generate_valid_slug()
        super(Listing, self).save(**kwargs)

    def _generate_valid_slug(self):
        if not self.is_valid_slug():
            slug = slugify(unidecode(self.Name))
            while Listing.objects.filter(slug=slug).exclude(bpm_id=self.bpm_id).exists():
                slug_parts = slug.split('-')
                if slug_parts[-1].isdigit():
                    slug_parts[-1] = '%s' % (int(slug_parts[-1]) + 1)
                else:
                    slug_parts.append('2')
                slug = '-'.join(slug_parts)
            self.slug = slug

    def is_valid_slug(self):
        if self.slug is None or len(self.slug) < 10:
            return False
        match = re.match('[^\w\s-]', self.slug)
        if not match:
            return False
        return self.slug == slugify(self.slug)

    @property
    def absolute_url(self):
        return self.get_absolute_url()

    def get_absolute_url(self):
        return reverse('listing_details', args=[self.slug])

    def get_features(self):
        attributes = []
        for attribute in self.attributelisting_set.all():
            attribute_name = _(attribute.attribute.name)
            if attribute.attribute.validation == 'realestate.listing.utils.validation_simple':
                attributes.append('{0}: {1}'.format(attribute_name, attribute.value))
            elif attribute.attribute.validation == 'realestate.listing.utils.validation_yesno':
                attributes.append(attribute_name)
            else:
                if attribute.attribute.validation == 'realestate.listing.utils.validation_integer':
                    attributes.append('{0} {1}'.format(attribute.value, attribute_name))
                else:
                    attributes.append('{0:.2f} {1}'.format(Decimal(attribute.value), attribute_name))

        return attributes

    @property
    def nearby(self):
        return Listing.objects.active(location=self.location).exclude(id=self.id).order_by('?')

    @property
    def has_baths_or_beds(self):
        return self.should_have_beds or self.should_have_baths

    @property
    def suggested(self):
        qs = Listing.objects.active(type=self.type)

        price = self.price
        lh = price * .90
        rh = price * 1.10

        if self.has_baths_or_beds:
            if self.should_have_baths:
                qs = qs.filter(baths=self.baths)
            if self.should_have_beds:
                qs = qs.filter(beds=self.beds)

            if qs.count() == 0:
                qs = Listing.objects.active(type=self.type, price__range=(lh, rh))
        else:
            qs = qs.filter(price__range=(lh, rh))

        return qs.exclude(id=self.id).order_by('?')


class ListingImage(models.Model):
    bpm_id = models.CharField(max_length=50, primary_key=True)
    listing_id = models.ForeignKey(Listing, to_field='bpm_id', related_name="images", default="")
    position = models.PositiveSmallIntegerField(_('position'), default=99)
    link = models.CharField(_('link'), max_length=250, default="")
    timestamp = models.IntegerField(_('timestamp'), default=-1)

    def from_json(self, obj):
        self.bpm_id = obj['Id']
        if obj['Listing']:
            self.listing_id = Listing.objects.get(bpm_id=obj['Listing']['value'])
        else:
            return False
        self.position = obj['Position']
        self.link = obj['Link']
        self.timestamp = int(time.time())
        return True

    ordering = ['position']

    @property
    def absolute_url(self):
        try:
            return self.link
        except ValueError:
            return ''

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Image')
        verbose_name_plural = _('Images')


class Amenity(models.Model):
    bpm_id = models.CharField(_('bpm_id'), max_length=50, unique=True)
    timestamp = models.IntegerField(_('timestamp'))
    Name = models.CharField(max_length=200)

    def from_json(self, obj):
        self.bpm_id = obj['Id']
        self.timestamp = int(time.time())
        self.Name = obj['Name']
        return True


class AmenityInObject(models.Model):
    bpm_id = models.CharField(_('bpm_id'), max_length=50, unique=True, default="", primary_key=True)
    timestamp = models.IntegerField(_('timestamp'))
    Amenity = models.ForeignKey(Amenity, to_field='bpm_id')
    Value = models.CharField(max_length=200, default="")
    IntValue = models.IntegerField()
    FloatValue = models.FloatField()
    BooleanValue = models.BooleanField()
    StringValue = models.CharField(max_length=500, default="")
    Listing = models.ForeignKey(Listing, to_field='bpm_id', related_name="amenityinobj")
    DateValue = models.CharField(max_length=50, default="")

    def from_json(self, obj):
        self.bpm_id = obj['Id']
        self.timestamp = int(time.time())
        if obj['Amenity']:
            self.Amenity = Amenity.objects.get(bpm_id=obj['Amenity']['value'])
        self.Value = obj['Value']
        self.IntValue = obj['IntValue']
        self.FloatValue = obj['FloatValue']
        self.BooleanValue = obj['BooleanValue']
        self.StringValue = obj['StringValue']
        if obj['Listing']:
            self.Listing = Listing.objects.get(bpm_id=obj['Listing']['value'])
        else:
            return False
        self.DateValue = obj['DateValue']
        return True
