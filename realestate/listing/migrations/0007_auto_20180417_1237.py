# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('listing', '0006_auto_20180416_2053'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='listing',
            options={'ordering': ['-Price'], 'verbose_name': 'Listing', 'verbose_name_plural': 'Listings'},
        ),
        migrations.AddField(
            model_name='listing',
            name='UsrBOOLEANIsPrivate',
            field=models.BooleanField(default=False),
            preserve_default=False,
        ),
    ]
