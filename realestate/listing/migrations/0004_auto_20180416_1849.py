# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('listing', '0003_auto_20180412_1408'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='deal',
            name='listing',
        ),
        migrations.AlterModelOptions(
            name='listingimage',
            options={'verbose_name': 'Image', 'verbose_name_plural': 'Images'},
        ),
        migrations.RemoveField(
            model_name='listing',
            name='active',
        ),
        migrations.RemoveField(
            model_name='listing',
            name='agent',
        ),
        migrations.RemoveField(
            model_name='listing',
            name='baths',
        ),
        migrations.RemoveField(
            model_name='listing',
            name='beds',
        ),
        migrations.RemoveField(
            model_name='listing',
            name='contact',
        ),
        migrations.RemoveField(
            model_name='listing',
            name='coords',
        ),
        migrations.RemoveField(
            model_name='listing',
            name='created_at',
        ),
        migrations.RemoveField(
            model_name='listing',
            name='description',
        ),
        migrations.RemoveField(
            model_name='listing',
            name='featured',
        ),
        migrations.RemoveField(
            model_name='listing',
            name='last_modified',
        ),
        migrations.RemoveField(
            model_name='listing',
            name='location',
        ),
        migrations.RemoveField(
            model_name='listing',
            name='notes',
        ),
        migrations.RemoveField(
            model_name='listing',
            name='offer',
        ),
        migrations.RemoveField(
            model_name='listing',
            name='price',
        ),
        migrations.RemoveField(
            model_name='listing',
            name='price_currency',
        ),
        migrations.RemoveField(
            model_name='listing',
            name='size',
        ),
        migrations.RemoveField(
            model_name='listing',
            name='title',
        ),
        migrations.RemoveField(
            model_name='listing',
            name='type',
        ),
        migrations.RemoveField(
            model_name='listingimage',
            name='added',
        ),
        migrations.RemoveField(
            model_name='listingimage',
            name='image',
        ),
        migrations.RemoveField(
            model_name='listingimage',
            name='listing',
        ),
        migrations.RemoveField(
            model_name='listingimage',
            name='name',
        ),
        migrations.RemoveField(
            model_name='listingimage',
            name='order',
        ),
        migrations.AddField(
            model_name='listing',
            name='Address',
            field=models.CharField(default=b'', max_length=50),
        ),
        migrations.AddField(
            model_name='listing',
            name='Description',
            field=models.CharField(default=b'', max_length=2000),
        ),
        migrations.AddField(
            model_name='listing',
            name='Latitude',
            field=models.CharField(default=b'', max_length=200),
        ),
        migrations.AddField(
            model_name='listing',
            name='ListingType',
            field=models.CharField(default=b'', max_length=50),
        ),
        migrations.AddField(
            model_name='listing',
            name='Longitude',
            field=models.CharField(default=b'', max_length=200),
        ),
        migrations.AddField(
            model_name='listing',
            name='Name',
            field=models.CharField(default=b'', max_length=50),
        ),
        migrations.AddField(
            model_name='listing',
            name='Notes',
            field=models.CharField(default=b'', max_length=2000),
        ),
        migrations.AddField(
            model_name='listing',
            name='Price',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='listing',
            name='PropertyCategory',
            field=models.CharField(default=b'', max_length=50),
        ),
        migrations.AddField(
            model_name='listing',
            name='PropertyType',
            field=models.CharField(default=b'', max_length=50),
        ),
        migrations.AddField(
            model_name='listing',
            name='Status',
            field=models.CharField(default=b'', max_length=50),
        ),
        migrations.AddField(
            model_name='listing',
            name='UsrRoad',
            field=models.CharField(default=b'', max_length=50),
        ),
        migrations.AddField(
            model_name='listing',
            name='bpm_id',
            field=models.CharField(default=b'', unique=True, max_length=50),
        ),
        migrations.AddField(
            model_name='listingimage',
            name='link',
            field=models.CharField(default=b'', max_length=250, verbose_name='link'),
        ),
        migrations.AddField(
            model_name='listingimage',
            name='listing_id',
            field=models.ForeignKey(to='listing.Listing', default=b'', to_field=b'bpm_id'),
        ),
        migrations.AddField(
            model_name='listingimage',
            name='pos',
            field=models.PositiveSmallIntegerField(default=99, verbose_name='position'),
        ),
        migrations.AddField(
            model_name='listingimage',
            name='timestamp',
            field=models.IntegerField(default=-1, verbose_name='timestamp'),
        ),
        migrations.DeleteModel(
            name='Deal',
        ),
    ]
