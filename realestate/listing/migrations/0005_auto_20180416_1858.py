# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('listing', '0004_auto_20180416_1849'),
    ]

    operations = [
        migrations.CreateModel(
            name='Amenity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('bpm_id', models.CharField(unique=True, max_length=50, verbose_name='bpm_id')),
                ('timestamp', models.IntegerField(verbose_name='timestamp')),
                ('Name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='AmenityInObject',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('bpm_id', models.CharField(default=b'', unique=True, max_length=50, verbose_name='bpm_id')),
                ('timestamp', models.IntegerField(verbose_name='timestamp')),
                ('PropertyId', models.CharField(default=b'', max_length=50)),
                ('Value', models.CharField(default=b'', max_length=200)),
                ('IntValue', models.IntegerField()),
                ('FloatValue', models.FloatField()),
                ('BooleanValue', models.BooleanField()),
                ('StringValue', models.CharField(default=b'', max_length=500)),
                ('DateValue', models.CharField(default=b'', max_length=50)),
                ('AmenityId', models.ForeignKey(to='listing.Amenity', to_field=b'bpm_id')),
                ('ListingId', models.ForeignKey(to='listing.Listing', to_field=b'bpm_id')),
            ],
        ),
        migrations.AlterField(
            model_name='listingimage',
            name='listing_id',
            field=models.ForeignKey(related_name='images', default=b'', to_field=b'bpm_id', to='listing.Listing'),
        ),
    ]
