# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import sorl.thumbnail.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Agent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=30, verbose_name='First name')),
                ('last_name', models.CharField(max_length=30, verbose_name='Last name')),
                ('phone', models.CharField(max_length=15, null=True, verbose_name='Phone', blank=True)),
                ('mobile', models.CharField(max_length=15, null=True, verbose_name='Cellphone', blank=True)),
                ('address', models.CharField(max_length=200, null=True, verbose_name='Address', blank=True)),
                ('image', sorl.thumbnail.fields.ImageField(default=b'', upload_to=b'agents/', null=True, verbose_name='Picture', blank=True)),
                ('active', models.BooleanField(default=False, verbose_name='Active')),
            ],
            options={
                'verbose_name': 'Agent',
                'verbose_name_plural': 'Agents',
            },
        ),
        migrations.CreateModel(
            name='Amenity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('bpm_id', models.CharField(unique=True, max_length=50, verbose_name='bpm_id')),
                ('timestamp', models.IntegerField(verbose_name='timestamp')),
                ('Name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='AmenityInObject',
            fields=[
                ('bpm_id', models.CharField(primary_key=True, default=b'', serialize=False, max_length=50, unique=True, verbose_name='bpm_id')),
                ('timestamp', models.IntegerField(verbose_name='timestamp')),
                ('Value', models.CharField(default=b'', max_length=200)),
                ('IntValue', models.IntegerField()),
                ('FloatValue', models.FloatField()),
                ('BooleanValue', models.BooleanField()),
                ('StringValue', models.CharField(default=b'', max_length=500)),
                ('DateValue', models.CharField(default=b'', max_length=50)),
                ('Amenity', models.ForeignKey(to='listing.Amenity', to_field=b'bpm_id')),
            ],
        ),
        migrations.CreateModel(
            name='Listing',
            fields=[
                ('bpm_id', models.CharField(default=b'', max_length=50, unique=True, serialize=False, primary_key=True)),
                ('Name', models.CharField(default=b'', max_length=2000)),
                ('Status', models.CharField(default=b'', max_length=50)),
                ('ListingType', models.CharField(default=b'', max_length=50)),
                ('Price', models.IntegerField(default=0)),
                ('Notes', models.CharField(default=b'', max_length=2000)),
                ('PropertyCategory', models.CharField(default=b'', max_length=50)),
                ('PropertyType', models.CharField(default=b'', max_length=50)),
                ('Description', models.CharField(default=b'', max_length=2000)),
                ('Address', models.CharField(default=b'', max_length=2000)),
                ('Latitude', models.CharField(default=b'', max_length=200)),
                ('Longitude', models.CharField(default=b'', max_length=200)),
                ('UsrRoad', models.CharField(default=b'', max_length=200)),
                ('UsrVerdict', models.CharField(default=b'', max_length=2000)),
                ('UsrBOOLEANIsPrivate', models.BooleanField()),
                ('UsrMaterial', models.IntegerField()),
                ('slug', models.SlugField(unique=True, max_length=200, verbose_name='Slug')),
                ('pricecriteria', models.IntegerField()),
                ('distancecriteria', models.IntegerField()),
                ('UsrOriginalID', models.CharField(default=b'', max_length=200)),
            ],
            options={
                'ordering': ['-Price'],
                'verbose_name': 'Listing',
                'verbose_name_plural': 'Listings',
            },
        ),
        migrations.CreateModel(
            name='ListingImage',
            fields=[
                ('bpm_id', models.CharField(max_length=50, serialize=False, primary_key=True)),
                ('position', models.PositiveSmallIntegerField(default=99, verbose_name='position')),
                ('link', models.CharField(default=b'', max_length=250, verbose_name='link')),
                ('timestamp', models.IntegerField(default=-1, verbose_name='timestamp')),
                ('listing_id', models.ForeignKey(related_name='images', default=b'', to='listing.Listing')),
            ],
            options={
                'verbose_name': 'Image',
                'verbose_name_plural': 'Images',
            },
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=60, verbose_name='Name')),
                ('location_type', models.CharField(default=b'sector', max_length=20, verbose_name='Location Type', choices=[(b'street', 'Street'), (b'sector', 'Sector'), (b'city', 'City'), (b'state', 'State/Province')])),
                ('parent', models.ForeignKey(verbose_name='Location', blank=True, to='listing.Location', null=True)),
            ],
        ),
        migrations.AddField(
            model_name='amenityinobject',
            name='Listing',
            field=models.ForeignKey(related_name='amenityinobj', to='listing.Listing'),
        ),
        migrations.AddField(
            model_name='agent',
            name='location',
            field=models.ForeignKey(verbose_name='Location', blank=True, to='listing.Location', null=True),
        ),
        migrations.AddField(
            model_name='agent',
            name='user',
            field=models.OneToOneField(null=True, blank=True, to=settings.AUTH_USER_MODEL, verbose_name='User'),
        ),
    ]
