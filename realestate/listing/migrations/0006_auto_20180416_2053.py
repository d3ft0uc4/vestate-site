# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('listing', '0005_auto_20180416_1858'),
    ]

    operations = [
        migrations.RenameField(
            model_name='amenityinobject',
            old_name='AmenityId',
            new_name='Amenity',
        ),
        migrations.RenameField(
            model_name='amenityinobject',
            old_name='ListingId',
            new_name='Listing',
        ),
        migrations.RenameField(
            model_name='listingimage',
            old_name='pos',
            new_name='position',
        ),
        migrations.RemoveField(
            model_name='amenityinobject',
            name='PropertyId',
        ),
        migrations.AddField(
            model_name='listingimage',
            name='bpm_id',
            field=models.CharField(default='', max_length=50),
            preserve_default=False,
        ),
    ]
