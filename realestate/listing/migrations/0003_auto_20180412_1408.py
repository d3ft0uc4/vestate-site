# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import djmoney.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('listing', '0002_auto_20151203_1455'),
    ]

    operations = [
        migrations.AlterField(
            model_name='deal',
            name='price_currency',
            field=djmoney.models.fields.CurrencyField(default='XYZ', max_length=3, editable=False, choices=[(b'RUB', 'Russian Ruble'), (b'USD', 'US Dollar')]),
        ),
        migrations.AlterField(
            model_name='listing',
            name='price_currency',
            field=djmoney.models.fields.CurrencyField(default='XYZ', max_length=3, editable=False, choices=[(b'RUB', 'Russian Ruble'), (b'USD', 'US Dollar')]),
        ),
    ]
