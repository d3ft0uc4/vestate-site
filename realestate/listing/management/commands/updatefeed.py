from django.core.management.base import BaseCommand
from realestate.listing.cian.cian import create_xml, get_data
from realestate.services import sync


class Command(BaseCommand):
    def handle(self, *args, **options):
        data = get_data()
        create_xml(data,tpe='UsrBOOLEANIsPublic')
        create_xml(data, tpe='UsrBOOLEANIsPrivate')
