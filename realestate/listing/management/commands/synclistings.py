from django.core.management.base import BaseCommand, CommandError

from realestate.services import sync


class Command(BaseCommand):
    def handle(self, *args, **options):
        sync(None)
