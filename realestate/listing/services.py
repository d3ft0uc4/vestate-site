import math

from decimal import Decimal


def calc_distance(search, obj):
    acc = 1
    d = int(float(search['distance']))
    v = int(float(search['vector']))
    price_search = int(float(search['price']))
    material = int(float(search['material']))
    if d != 5:
        distance_actual = obj.distancecriteria
        acc += math.pow(distance_actual - d, 2)
    if material != 5:
        acc += math.pow(10 - obj.UsrMaterial - material, 2)
    if v != 5:
        vector_actual = get_vector(obj)
        acc += math.pow(vector_actual - v, 2)
    if price_search != 5:
        price_actual = obj.pricecriteria
        acc += math.pow(price_search - price_actual, 2)
    if obj.usrratingcoefficient > 0:
        acc *= float(Decimal(1.0) / obj.usrratingcoefficient)
    return acc, obj.bpm_id

def calc_distance2(search, obj):
    acc = 1
    d = int(float(search['distance']))
    v = int(float(search['vector']))
    price_search = int(float(search['price']))
    material = int(float(search['material']))
    if d != 5:
        distance_actual = obj.distancecriteria
        acc += math.pow(distance_actual - d, 2)
    if material != 5:
        acc += math.pow(10 - obj.UsrMaterial - material, 2)
    if v != 5:
        vector_actual = get_vector(obj)
        acc += math.pow(vector_actual - v, 2)
    if price_search != 5:
        price_actual = obj.pricecriteria
        acc += math.pow(price_search - price_actual, 2)
    if obj.usrratingcoefficient > 0:
        acc *= float(Decimal(1.0) / obj.usrratingcoefficient)
    return acc, obj.bpm_id


def get_vector(obj):
    try:
        lat = float(obj.Latitude)
        lon = float(obj.Longitude)
        min_riga = min(map(lambda c: get_distance((lat, lon), c), riga))
        min_rubl = min(map(lambda c: get_distance((lat, lon), c), rublevka))
        d = (min_riga / min_rubl) * 5
        if d > 10:
            d = 10
        return d
    except:
        return 5


def get_distance(c1, c2):
    return math.sqrt(math.pow(c1[0] - c2[0], 2) + math.pow(c1[1] - c2[1], 2))


riga = [
    (55.8029722, 37.3073402),
    (55.8037192, 37.2888442),
    (55.7992563, 37.2713782),
    (55.7941643, 37.2563032),
    (55.7859113, 37.2401242),
    (55.7774882, 37.2223572),
    (55.7746633, 37.1954492),
    (55.7794173, 37.1673822),
    (55.7847743, 37.1336932),
    (55.7863173, 37.1024512),
    (55.7850373, 37.0725822),
    (55.7871113, 37.0533562),
    (55.8022372, 37.0321562),
    (55.8153072, 37.0144322),
    (55.8189472, 36.9750362),
    (55.8192322, 36.9374822),
    (55.8245692, 36.9030032),
    (55.8290762, 36.8675552),
    (55.8372942, 36.8316782),
    (55.8460402, 36.8098772),
    (55.8788262, 36.6679432),
    (55.9158452, 36.5896402),
    (55.9616962, 36.4973672),
    (55.9803562, 36.4427232)
]

rublevka = [
    (55.7637593, 37.3537592),
    (55.7556453, 37.3311422),
    (55.7499683, 37.3187392),
    (55.7446053, 37.2856942),
    (55.7385893, 37.2566402),
    (55.7369093, 37.2245272),
    (55.7287653, 37.1980492),
    (55.7233753, 37.1607982),
    (55.7202922, 37.1271292),
    (55.7170453, 37.1078332),
    (55.7157183, 37.0842062),
    (55.7148973, 37.0684992)
]
