# coding=utf-8
from django.conf import settings
from django.utils.functional import SimpleLazyObject

BPM_BASE_URL = getattr(settings, 'BPM_BASE_URL')
BPM_USERNAME = getattr(settings, 'BPM_USERNAME')
BPM_PASSWORD = getattr(settings, 'BPM_PASSWORD')

GMAIL_USER = getattr(settings, 'GMAIL_USER')
GMAIL_PASSWORD = getattr(settings, 'GMAIL_PASSWORD')

MAIL_RECEIVER = getattr(settings, 'MAIL_RECEIVER')

DEBUG = getattr(settings, 'DEBUG')

FEED_PATH = getattr(settings, 'FEED_PATH')

SYNC_KEY = getattr(settings, 'SYNC_KEY')
