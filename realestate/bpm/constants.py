# coding=utf-8
from enum import Enum


class OperationTypes(Enum):
    SELECT = 0
    INSERT = 1
    UPDATE = 2
    DELETE = 3
    BATCH = 4


class ExpressionTypes(Enum):
    SchemaColumn = 0
    Function = 1
    Parameter = 2
    SubQuery = 3
    ArithmeticOperation = 4


expression_types = {
    "SchemaColumn": 0,
    "Function": 1,
    "Parameter": 2,
    "SubQuery": 3,
    "ArithmeticOperation": 4
}
data_value_types = {
    "Guid": 0,
    "Text": 1,
    "Integer": 4,
    "Float": 5,
    "Money": 6,
    "DateTime": 7,
    "Date": 8,
    "Time": 9,
    "Lookup": 10,
    "Enum": 11,
    "Boolean": 12,
    "Blob": 13,
    "Image": 14,
    "ImageLookup": 16,
    "Color": 18,
    "Mapping": 26
}

headers = {'Content-Type': 'application/json'}


def get_file_upload_url(base_url):
    return '{0}/0/rest/FileApiService/Upload'.format(base_url)


def get_base_view(base_url):
    return '{0}/0/Nui/ViewModule.aspx'.format(base_url)


def get_auth_url(base_url):
    return '{0}/ServiceModel/AuthService.svc/Login'.format(base_url)


def get_query_endpoint(base_url, operation_type):
    return '{0}/0/DataService/json/reply/{1}Query'.format(base_url, operation_type)


def get_sys_image_url(base_url, guid):
    return '{0}/0/img/entity/hash/SysImage/Data/{1}'.format(base_url, guid)
