"use strict";
/*-----------------------------------------
 NoUiSlider - Property Fields

 1. Price Range
 2. Area Size
 3. Lot Size
 4. Year Built
 ------------------------------------------*/


// 1. Price Range
var propertyPriceRange = document.getElementById('property-price-range');

if ($('#property-price-range')[0]) {
    var propertyPriceRangeValues = [
        document.getElementById('property-price-upper'),
        document.getElementById('property-price-lower')
    ]

    noUiSlider.create (propertyPriceRange, {
        start: [1, 5000],
        connect: true,
        range: {
            'min': 1,
		    '50%': 50,
            '66%': 150,
            'max': 5000
        },
        format: wNumb({
            decimals: 0,
            thousand: ' ',
            suffix: ' млн. рублей'
        })
    });

    propertyPriceRange.noUiSlider.on('update', function (values, handle) {
        propertyPriceRangeValues[handle].innerHTML = values[handle];
    });
}

// 2. Property Area Size
var vector = document.getElementById('vector-slider');
if ($('#vector-slider')[0]) {
    noUiSlider.create (vector, {
        start: [5],
        range: {
            'min': 0,
            'max': 10
        },
        step: 1
    });


}

// 3. Lot Size
var distance = document.getElementById('distance-slider');
if ($('#distance-slider')[0]) {
    noUiSlider.create (distance, {
        start: [5],
        range: {
            'min': 0,
            'max': 10
        },
        step: 1
    });


}

var material = document.getElementById('material-slider');

if ($('#material-slider')[0]) {
    noUiSlider.create (material, {
        start: [5],
        range: {
            'min': 0,
            'max': 10
        },
        step: 1
    });

}

var price = document.getElementById('price-slider');

if ($('#price-slider')[0]) {
    noUiSlider.create (price, {
        start: [5],
        range: {
            'min': 0,
            'max': 10
        },
        step: 1
    });

}

$(document).ready(function () {
    /*-----------------------------------------------------
     Submit property steps switch
     - used in last form tab of 'submit-property.html'
     ------------------------------------------------------*/
    $('body').on('shown.bs.tab', '.submit-property__button', function () {
        var currentTab = $(this).attr('href');

        $('.submit-property__steps > li').removeClass('active');
        $('.submit-property__steps > li > a[href=' + currentTab + ']').parent().addClass('active');
    })


    $('.search-btn').click(function (e) {
        e.preventDefault();
        var range = propertyPriceRange.noUiSlider.get();
        $.ajax({
            url: '/search/',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({
                'vector': vector.noUiSlider.get(),
                'distance': distance.noUiSlider.get(),
                'material': material.noUiSlider.get(),
                'price': price.noUiSlider.get(),
                'lower': range[0],
                'upper': range[1]

            }),
            dataType: 'text',
            success: function () {
                window.location.href = '/listings/';
            },
            fail: function () {

            }
        });

    });

    /*-----------------------------------------------------
     Demo list delete
     - Used in dashboaed/listings.html
     ------------------------------------------------------*/
    if ($('[data-demo-action="delete-listing"]')[0]) {
        $('[data-demo-action="delete-listing"]').click(function (e) {
            e.preventDefault();

            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function () {
                swal(
                    'Deleted!',
                    'Your list has been deleted.',
                    'success'
                );
            })
        });
    }

});
     if($('.team-slider')[0]) {
         $('.team-slider .team-grid').slick({
             speed: 300,
             slidesToShow: 4,
             slidesToScroll: 1,
             dotsClass: 'slick-dots slick-dots-light',
             infinite: true,
             responsive: [
                 {
                     breakpoint: 1200,
                     settings: {
                         slidesToShow: 2,
                         slidesToScroll: 3,
                     }
                 },
                 {
                     breakpoint: 960,
                     settings: {
                         slidesToShow: 2,
                         slidesToScroll: 2
                     }
                 },
                 {
                     breakpoint: 700,
                     settings: {
                         slidesToShow: 2,
                         slidesToScroll: 2,
                         dots: true,
                         arrows: false
                     }
                 },
                 {
                     breakpoint: 550,
                     settings: {
                         slidesToShow: 1,
                         slidesToScroll: 1,
                         dots: true,
                         arrows: false
                     }
                 }
             ]
         });
     }

     if($('.testimonials-slider')[0]) {
         $('.testimonials-slider .testimonials-grid').slick({
             speed: 300,
             slidesToShow: 1,
             slidesToScroll: 1,
             dotsClass: 'slick-dots slick-dots-light',
             infinite: true,
             responsive: [
                 {
                     breakpoint: 1200,
                     settings: {
                         slidesToShow: 1,
                         slidesToScroll: 1,
                     }
                 },
                 {
                     breakpoint: 960,
                     settings: {
                         slidesToShow: 1,
                         slidesToScroll: 1
                     }
                 },
                 {
                     breakpoint: 700,
                     settings: {
                         slidesToShow: 1,
                         slidesToScroll: 1,
                         dots: true,
                         arrows: false
                     }
                 },
                 {
                     breakpoint: 550,
                     settings: {
                         slidesToShow: 1,
                         slidesToScroll: 1,
                         dots: true,
                         arrows: false
                     }
                 }
             ]
         });
     }

