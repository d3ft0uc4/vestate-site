import os
import six

root_path = os.path.abspath(os.path.dirname(__file__) + '/../')
activate_this = os.path.join(root_path, "venv/bin/activate_this.py")
if six.PY3:
    exec (open(activate_this).read())
else:
    execfile(activate_this, dict(__file__=activate_this))

# from django.core.handlers.wsgi import WSGIHandler

os.environ['DJANGO_SETTINGS_MODULE'] = 'vestate.settings'
from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()
