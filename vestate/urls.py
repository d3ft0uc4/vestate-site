from django.conf.urls import *
from django.conf import settings


# handler404 = 'realestate.home.views.handle404'

urlpatterns = patterns(
    '',
    url('^', include('realestate.urls'))
)

# if settings.DEBUG:
urlpatterns += patterns(
    '',
    (r'^media/(.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    (r'^static/(.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
)
